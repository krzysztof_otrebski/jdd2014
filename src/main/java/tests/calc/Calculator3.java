package tests.calc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.HashMap;
import java.util.concurrent.*;

public class Calculator3 {

  private static final Logger LOGGER = LoggerFactory.getLogger(Calculator3.class);

  static ExecutorService executorService = Executors.newSingleThreadExecutor();

  public Future<Integer> add(final int n1, final int n2) throws ExecutionException, InterruptedException {
    LOGGER.info("Adding " + n1 + " to " + n2);
    Callable<Integer> callable = new MdcCallable<Integer>() {
      @Override
      public Integer doCallable() throws Exception {
        Thread.sleep(100);
        LOGGER.info("executing callable");
        return n1 + n2;
      }
    };

    return executorService.submit(callable);
  }


  public abstract static class MdcCallable<T> implements Callable<T> {

    private HashMap<String, String> mdcMap;

    protected MdcCallable() {
      mdcMap = new HashMap<>(MDC.getCopyOfContextMap());
    }

    @Override
    public final T call() throws Exception {
      MDC.setContextMap(mdcMap);
      try {
        return doCallable();
      } finally {
        mdcMap.keySet().forEach((c) -> MDC.remove(c.toLowerCase()));
      }
    }

    public abstract T doCallable() throws Exception;
  }
}
