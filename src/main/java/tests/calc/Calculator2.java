package tests.calc;

import org.slf4j.Logger; import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Calculator2 {

    private static final Logger LOGGER = LoggerFactory.getLogger(Calculator2.class);

    static ExecutorService executorService = Executors.newSingleThreadExecutor();

    public Future<Integer> add(final int n1, final int n2) throws ExecutionException, InterruptedException {
        LOGGER.info("Adding " + n1 + " to " + n2);
        Callable<Integer> callable = new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
              Thread.sleep(100);
                LOGGER.info("executing callable");
                return n1 + n2;
            }
        };
      return executorService.submit(callable);
    }
}
