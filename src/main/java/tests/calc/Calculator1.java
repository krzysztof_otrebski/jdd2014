package tests.calc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Calculator1 {

 private static final Logger LOGGER = LoggerFactory.getLogger(Calculator1.class);

  public int add(int n1, int n2){
    LOGGER.info("Adding " + n1 + " to " + n2);
    final int result = n1 + n2;
    LOGGER.info("Result is "+ result);
    return result;
  }

}
