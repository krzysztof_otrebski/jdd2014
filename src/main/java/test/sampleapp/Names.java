package test.sampleapp;

import com.google.common.base.Splitter;

import java.util.Collection;

public class Names {
    private static final String names =
            "Gary\n" +
            "Thad\n" +
            "Suk\n" +
            "Allyson\n" +
            "Curt\n" +
            "Theodora\n" +
            "Francisco\n" +
            "Freida\n" +
            "Tamika\n" +
            "Karmen\n" +
            "Alvera\n" +
            "Maryjo\n" +
            "Rosetta\n" +
            "Deandre\n" +
            "Theron\n" +
            "Dominick\n" +
            "Lacey\n" +
            "Cecile\n" +
            "Tonya\n" +
            "Pansy\n" +
            "Claire\n" +
            "Mia\n" +
            "Vicente\n" +
            "Vanessa\n" +
            "Cheri\n" +
            "Alice\n" +
            "Lane\n" +
            "Gilbert";

    public static Collection<String> getNames(){
        return Splitter.on('\n').splitToList(names);
    }
}
