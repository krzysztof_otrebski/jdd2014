package test.sampleapp.services.tickets;

import com.google.common.io.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test.sampleapp.services.Result;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Random;

public class TicketService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TicketService.class);

    final String request;
    final String response;

    public TicketService() throws IOException {
        request = Resources.toString(TicketService.class.getResource("request.json"), Charset.forName("UTF-8"));
        response = Resources.toString(TicketService.class.getResource("response.json"), Charset.forName("UTF-8"));
    }

    public Result getTickets() throws IOException {
        LOGGER.debug("-> Tickets " + request);
        Random random = new Random();
        try {
            Thread.sleep(200 + random.nextInt(300));
        } catch (InterruptedException e) {
            //Ignore
        }
        LOGGER.debug("<- Tickets " + response);
        return new Result();
    }
}
