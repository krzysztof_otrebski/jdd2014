package test.sampleapp.services.payment;

import com.google.common.io.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test.sampleapp.services.Result;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Random;

public class PaymentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentService.class);

    final String request;
    final String response;

    public PaymentService() throws IOException {
        request = Resources.toString(PaymentService.class.getResource("request.xml"), Charset.forName("UTF-8"));
        response = Resources.toString(PaymentService.class.getResource("response.xml"), Charset.forName("UTF-8"));
    }

    public Result performPayment() throws IOException {
        LOGGER.debug("-> Payment " + request );
        Random random = new Random();

        try {
            Thread.sleep(random.nextInt(100));
        } catch (InterruptedException e) {
            //Ignore
        }
        LOGGER.debug("<- Payment " + response );
        return new Result();
    }
}
