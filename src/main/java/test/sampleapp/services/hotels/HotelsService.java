package test.sampleapp.services.hotels;

import com.google.common.io.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test.sampleapp.services.Result;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Random;

public class HotelsService {

  private static final Logger LOGGER = LoggerFactory.getLogger(HotelsService.class);

  final String request;
  final String response;

  public HotelsService() throws IOException {
    request = Resources.toString(HotelsService.class.getResource("request.xml"), Charset.forName("UTF-8"));
    response = Resources.toString(HotelsService.class.getResource("response.xml"), Charset.forName("UTF-8"));
  }

  public Result getHotels() throws IOException {
    LOGGER.debug("-> Hotels " + request);
    Random random = new Random();
    try {
      Thread.sleep(200 + random.nextInt(300));
      Random r = new Random();
      if (r.nextInt(100) == 1) {
        throw new IOException("Error executing request, connection broken.... :)");
      }
    } catch (InterruptedException e) {
      //Ignore
    }
    LOGGER.debug("<- Hotels " + response);
    return new Result();
  }
}
