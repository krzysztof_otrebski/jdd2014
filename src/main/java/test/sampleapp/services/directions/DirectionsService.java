package test.sampleapp.services.directions;

import com.google.common.io.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test.sampleapp.services.Result;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Random;


public class DirectionsService {

  private static final Logger LOGGER = LoggerFactory.getLogger(DirectionsService.class);

  final String request;
  final String response;

  public DirectionsService() throws IOException {
    request = Resources.toString(DirectionsService.class.getResource("request.xml"), Charset.forName("UTF-8"));
    response = Resources.toString(DirectionsService.class.getResource("response.xml"), Charset.forName("UTF-8"));
  }

  public Result getDirections() throws IOException {
    LOGGER.debug("-> Directions " + request);
    Random random = new Random();
    try {
      Thread.sleep(100 + random.nextInt(200));
    } catch (InterruptedException e) {
      //Ignore
    }
    LOGGER.debug("<- Directions" + response);
    return new Result();
  }

  public static void main(String[] args) throws IOException {
    new DirectionsService().getDirections();
  }
}
