package test.sampleapp.services.wheater;

import com.google.common.io.Resources;
import org.slf4j.Logger; import org.slf4j.LoggerFactory;
import test.sampleapp.services.Result;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Random;

public class MeteoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MeteoService.class);

    final String request;
    final String response;

    public MeteoService() throws IOException {
        request = Resources.toString(MeteoService.class.getResource("request.xml"), Charset.forName("UTF-8"));
        response = Resources.toString(MeteoService.class.getResource("response.xml"), Charset.forName("UTF-8"));
    }

    public Result getMeteo() throws IOException {
        LOGGER.debug("-> Meteo " + request);
        Random random = new Random();
        try {
            Thread.sleep(200 + random.nextInt(300));
        } catch (InterruptedException e) {
            //Ignore
        }
        LOGGER.debug("<- Meteo " + response);
        return new Result();
    }
}
