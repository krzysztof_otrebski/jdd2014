package test.sampleapp;

import org.slf4j.Logger; import org.slf4j.LoggerFactory;
import test.sampleapp.services.Result;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class SampleAppMultiThreaded extends SampleApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(SampleAppMultiThreaded.class);
    private final ExecutorService executorService;

    public SampleAppMultiThreaded() throws IOException {
        executorService = Executors.newFixedThreadPool(50);
    }


    @Override
    protected void performRequests() throws Exception {
        List<Future<Result>> list = new ArrayList<>();
        list.add(executorService.submit(directionsService::getDirections));
        list.add(executorService.submit(hotelsService::getHotels));
        list.add(executorService.submit(ticketService::getTickets));
        list.add(executorService.submit(meteoService::getMeteo));

        list.forEach((f) -> {
            try {
                f.get();
            } catch (InterruptedException | ExecutionException e) {
                LOGGER.error("Error executing in service",e);
                throw new RuntimeException(e);
            }
        });
    }


    @Override
    protected void performPayments() throws Exception {
      executorService.submit(paymentService::performPayment).get();
    }

    public static void main(String[] args) throws IOException {
        new SampleAppMultiThreaded().startApp();
    }
}
