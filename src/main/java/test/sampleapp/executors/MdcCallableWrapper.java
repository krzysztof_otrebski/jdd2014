package test.sampleapp.executors;


import org.slf4j.MDC;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.stream.Stream;

public class MdcCallableWrapper<T> implements Callable<T> {

  private HashMap<String, String> mdcMap;
  private Callable<T> callable;


  protected MdcCallableWrapper(Callable<T> callable) {
    this.callable = callable;
    mdcMap = new HashMap<>();
    final Set set = MDC.getCopyOfContextMap().keySet();
    set.forEach((key) -> {
          mdcMap.put(key.toString(), MDC.get(key.toString()));
        }
    );
  }

  @Override
  public final T call() throws Exception {
    mdcMap.keySet().forEach((c) -> MDC.put(c, mdcMap.get(c)));
    try {
      return callable.call();
    } finally {
      mdcMap.keySet().forEach((c) -> MDC.remove(c.toLowerCase()));
    }
  }


  public static <V> MdcCallableWrapper<V> wrap(Callable<V> callable) {
    return new MdcCallableWrapper(callable);
  }

  public static <V> Collection<MdcCallableWrapper<V>> wrap(Collection<? extends Callable<V>> collection) {
    Stream<MdcCallableWrapper<V>> map = collection.stream().map(i -> MdcCallableWrapper.wrap(i));
    ArrayList<MdcCallableWrapper<V>> arrayList = new ArrayList();
    map.forEach(arrayList::add);
    return arrayList;
  }
}
