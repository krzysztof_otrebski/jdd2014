package test.sampleapp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import test.sampleapp.services.Result;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.*;

public class SampleAppMultiThreadedFix extends SampleApp {

  private static final Logger LOGGER = LoggerFactory.getLogger(SampleAppMultiThreadedFix.class);
  private final ExecutorService executorService;

  public SampleAppMultiThreadedFix() throws IOException {
    super();
    executorService = Executors.newFixedThreadPool(50);
  }


  @Override
  protected void performRequests() throws Exception {

    List<Future<Result>> list = new ArrayList<>();
    list.add(executorService.submit(new MdcCallable<Result>() {
      @Override
      public Result doCallable() throws Exception {
        return directionsService.getDirections();
      }
    }));
    list.add(executorService.submit(new MdcCallable<Result>() {
      @Override
      public Result doCallable() throws Exception {
        return hotelsService.getHotels();
      }
    }));
    list.add(executorService.submit(new MdcCallable<Result>() {
      @Override
      public Result doCallable() throws Exception {
        return ticketService.getTickets();
      }
    }));
    list.add(executorService.submit(new MdcCallable<Result>() {
      @Override
      public Result doCallable() throws Exception {
        return meteoService.getMeteo();
      }
    }));

    list.forEach((f) -> {
      try {
        f.get();
      } catch (InterruptedException | ExecutionException e) {
        LOGGER.error("Error executing in service", e);
        throw new RuntimeException(e);
      }
    });

  }


  public interface MdcCallb<T> {
    public abstract T doCallable() throws Exception;
  }

  public abstract static class MdcCallable<T> implements Callable<T>, MdcCallb<T> {
    private HashMap<String, String> mdcMap;


    protected MdcCallable() {
      mdcMap = new HashMap<>(MDC.getCopyOfContextMap());
    }

    @Override
    public final T call() throws Exception {
      MDC.setContextMap(mdcMap);
      try {
        return doCallable();
      } finally {
        mdcMap.keySet().forEach((c) -> MDC.remove(c.toLowerCase()));
      }
    }

//        public abstract T doCallable() throws Exception;
  }

  public static void main(String[] args) throws IOException {
    new SampleAppMultiThreadedFix().startApp();
  }
}
