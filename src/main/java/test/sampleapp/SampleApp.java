package test.sampleapp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import test.sampleapp.services.directions.DirectionsService;
import test.sampleapp.services.hotels.HotelsService;
import test.sampleapp.services.payment.PaymentService;
import test.sampleapp.services.tickets.TicketService;
import test.sampleapp.services.wheater.MeteoService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;

public class SampleApp {

  private static final Logger LOGGER = LoggerFactory.getLogger(SampleApp.class);
  public static final String MDC_USER = "USER";
  public static final String MDC_REQ_ID = "REQ_ID";

  private LinkedBlockingDeque<String> userIds;
  protected DirectionsService directionsService;
  protected HotelsService hotelsService;
  protected PaymentService paymentService;
  protected TicketService ticketService;
  protected MeteoService meteoService;

  public SampleApp() throws IOException {
    directionsService = new DirectionsService();
    hotelsService = new HotelsService();
    paymentService = new PaymentService();
    ticketService = new TicketService();
    meteoService = new MeteoService();
    userIds = new LinkedBlockingDeque<>(Names.getNames());
  }


  public final void request() {
    String user = null;
    final long timestamp = System.currentTimeMillis();
    try {
      final String requestUuid = UUID.randomUUID().toString().substring(0, 8);
      MDC.put(MDC_REQ_ID, requestUuid);
      user = userIds.take();
      MDC.put(MDC_USER, user);

      performRequests();
      if (new Random().nextInt(100) == 0) {
        performPayments();
      }
      LOGGER.info("Sending response, processing have taken " + (System.currentTimeMillis() - timestamp) + "ms");
    } catch (Exception e) {
      e.printStackTrace();
      LOGGER.error(String.format("Error on serving request"), e);
    } finally {
      MDC.remove(MDC_REQ_ID);
      MDC.remove(MDC_USER);
      if (user != null) {
        userIds.offer(user);
      }
    }
  }

  protected void performPayments() throws IOException, Exception {
    paymentService.performPayment();
  }

  protected void performRequests() throws Exception {
    directionsService.getDirections();
    hotelsService.getHotels();
    ticketService.getTickets();
    meteoService.getMeteo();
  }

  public final void startApp() {
    final long start = System.currentTimeMillis();

    final int nThreads = 10;
    final int count = 10000;
    final ExecutorService executorService = Executors.newFixedThreadPool(nThreads);
    LOGGER.info("{} threads will be used to handle {} tasks",nThreads,count);

    ArrayList<Future<?>> futures = new ArrayList<>(count);

    for (int i = 0; i < count; i++) {
      LOGGER.info("Submitting task number {} to execute", i);
      futures.add(executorService.submit(this::request));
      executorService.submit(() -> {
        try {
          Thread.sleep(new Random().nextInt(5 * 1000));
        } catch (Exception e) {
        }
      });
    }
    LOGGER.info(String.format("Have submitted %d futures",count));


    futures.forEach((f) -> {
      try {
        f.get();
      } catch (Exception e) {/**/}
    });
    LOGGER.info("Shutting down application, application was running " + (System.currentTimeMillis()-start) + "ms");
    LOGGER.info("Finished with \"Success\"");
    executorService.shutdown();
    System.exit(0);
  }

  public static void main(String[] args) throws IOException {
    new SampleApp().startApp();
  }
}
