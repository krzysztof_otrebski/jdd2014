package test.lambda;


import com.google.common.base.Splitter;

import java.util.List;
import java.util.stream.Collectors;

public class LambdaLog {
    public static void main(String[] args) {
        final List<String> strings = Splitter.on(' ').splitToList("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sagittis mauris sed arcu feugiat lacinia. Etiam vel metus nisl. Duis elit mauris, maximus in rutrum vel, placerat quis quam. Praesent maximus erat justo, non pharetra arcu semper ut. Curabitur eu lacus a sapien interdum pulvinar nec id ex. Mauris lobortis, augue ac commodo elementum, diam mauris rutrum ligula, et vehicula risus nibh vitae elit. Aenean tincidunt, nibh sed porttitor porttitor, urna purus placerat sem, nec pellentesque purus nisi vel purus. Suspendisse dignissim suscipit consequat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas at laoreet velit. Quisque pharetra, nunc at blandit auctor, nibh orci tempus libero, eget consectetur neque arcu in mauris.");
        final List<Integer> collect = strings.stream().
                map(String::toUpperCase).
                map(String::length).
                map(i -> 4 * i).
                map(i -> i/(i-64)).
                collect(Collectors.toList());
        collect.forEach(System.out::println);
    }
}
