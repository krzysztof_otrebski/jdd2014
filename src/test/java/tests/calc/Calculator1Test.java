package tests.calc;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Calculator1Test {

  Calculator1 calculator1 = new Calculator1();

  @Test
  public void testAdd2To2() throws Exception {
    assertEquals(4, calculator1.add(2, 2));
  }

  @Test
  public void testAdd0To0() throws Exception {
    assertEquals(0, calculator1.add(0, 0));
  }

 
}