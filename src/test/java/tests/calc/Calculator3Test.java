package tests.calc;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Calculator3Test {

  Calculator3 calculator = new Calculator3();

  @Test
  public void testAdd2To2() throws Exception {
    assertEquals(4, calculator.add(2, 2).get().intValue());
  }

  @Test
  public void testAdd0To0() throws Exception {
    assertEquals(0, calculator.add(0, 0).get().intValue());
  }

  @Test
  public void testAdd0To1() throws Exception {
    assertEquals(1, calculator.add(0, 1).get().intValue());
  }

  @Test
  public void testAddMinus1ToMinus1() throws Exception {
    assertEquals(-2, calculator.add(-1, -1).get().intValue());
  }
}