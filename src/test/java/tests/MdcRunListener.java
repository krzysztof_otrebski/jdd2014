package tests;

import org.slf4j.MDC;
import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.RunListener;

public class MdcRunListener extends RunListener {

  private static final String MDC_FIELD = "test";
  private static final String MDC_TEST_NAME = "test-name";


  @Override
  public void testStarted(Description description) throws Exception {
    MDC.put(MDC_TEST_NAME,description.getClassName()+"."+description.getMethodName());
    MDC.put(MDC_FIELD,description.getClassName());
  }

  @Override
  public void testRunFinished(Result result) throws Exception {
    MDC.remove(MDC_TEST_NAME);
    MDC.remove(MDC_FIELD);
  }

  @Override
  public void testRunStarted(Description description) throws Exception {
  }
}
